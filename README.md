LINK DEPLOYED FRONTEND : https://todoapp-fe.vercel.app
LINK DEPLOYED BACKEND : https://todoapp-be.vercel.app

FEATURE :
1. Login page dengan 4 angka
2. Create , Update , dan Delete Task di to do list di tanggal/hari yang diinginkan

JAWABAN SOAL :
1. Fungsi dari JSON pada rest API adalah sebagai media / format data komunikasi antara backend dan frontend. Sebagai contoh jika frontend merequest sesuatu , backend akan mengirim response / jawaban dalam bentuk JSON kepada frontend.

2. REST API adalah sebuah pendekatan / gaya arsitektur transmisi data yang memudahkan client untuk melakukan permintaan kepada server. REST dibagi menjadi 4 metode / endpoint yaitu GET,POST,PUT,DELETE. Server akan menyediakan beberapa endpoint tersebut beserta controller / tugasnya masing - masing. Setelah itu client akan melakukan permintaan bisa dalam bentuk JSON (yang paling umum) , HTML , python , dll kepada server melalui salah satu metode enpoint yang disuguhkan. Endpoint yang dihit kemudian akan mengirimkan response / jawaban dalam bentuk JSON kepada client.
