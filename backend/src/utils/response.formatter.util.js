const responseDefaults = () => {
    const response = {
        'status': 'SUCCESS'
    }

    return response
}

module.exports = {
    responseSuccess: (message = null) => {
        const response = responseDefaults()
        
        response.status = 'SUCCESS'
        response.message = message

        return response
    },
    responseError: (message = null) => {
        const response = responseDefaults()

        response.status = 'FAILED'
        response.message = message

        return response
    }
}