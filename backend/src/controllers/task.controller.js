const { Task } = require('../../models')
const { responseSuccess, responseError } = require('../utils/response.formatter.util')

module.exports = {
    getAll: async (req,res) => {
        const user_id = req.params.user_id
        try{
            const allTask = await Task.findAll({
                where:{
                    user_id
                }
            })
            res.json(responseSuccess(allTask))            
        } catch(err){
            console.log(err.message)
            res.status(500).json(responseError('Failed to get tasks'))
        }
    },
    create: async (req, res) => {
        const {user_id,detail,status,fullDate} = req.body
        try {
            const newTask = {
                user_id,
                detail,
                status,
                fullDate,
                createdAt: new Date(),
                updatedAt: new Date()
            }
            await Task.create(newTask)
            
            res.json(responseSuccess('Success create task'))            
        } catch (err) {
            console.log(err.message)
            res.status(500).json(responseError('Failed create task'))
        }
    },
    edit: async (req , res) => {
        const task_id = req.params.task_id
        const {detail,status} = req.body
        try{
            await Task.update({
                detail,
                status
            },{
                where:{
                    id:task_id
                }
            })
            res.json(responseSuccess(`Success update task with id=${task_id}`))            
        } catch(err){
            console.log(err.message)
            res.status(500).json(responseError('Failed update task'))
        }
    },
    delete: async (req, res) => {
        try {
            const task_id = req.params.task_id
            const task = await Task.findOne({ where: { id:task_id } })
            if (!task) {
                res.status(404).json(responseError('Task not found'))
            } else {
                await Task.destroy({ where: { id:task_id } })
                res.json(responseSuccess(`Success delete task with id=${task_id}`))
            }
        } catch (err) {
            res.status(500).json(responseError('Delete user failed'))
        }
    }
}