require('dotenv').config()

const express = require('express')
const app = express()
const cors = require('cors')

const routes = require('./src/routes')

app.use(cors())
app.use(express.static('public'))
app.use(express.json())
app.use(express.urlencoded({ extended: true }))

// Routes
app.use(routes)

// Running Server
app.listen(process.env.PORT || 3001, () => {
    console.log(`Server Running. Listening on port http://localhost:${process.env.PORT || 3001}`)
})