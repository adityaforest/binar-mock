import { createSlice } from "@reduxjs/toolkit";
import { exampleThunk } from "../thunk/ExampleThunk";

const slice = createSlice({
    name:'exampleReducer',
    initialState:{
        counter: 0
    },
    reducers:{
        increaseCounter:(state,action) => {
            state.counter++
        },
        decreaseCounter:(state,action) => {
            state.counter--
        },
        setCounter:(state,action) => {
            state.counter = action.payload
        }
    },
    extraReducers: builder => {
        builder.addCase(exampleThunk.fulfilled, (state,action) => {
            console.log('redux extraReducer get this input : ' + action.payload)
            state.counter = action.payload
        })
        builder.addCase(exampleThunk.pending, (state,action) => {
            console.log('redux extraReducer is pending for exampleThunk')
        })
        builder.addCase(exampleThunk.rejected, (state,action) => {
            console.log('redux extraReducer get this error : ' + action.error)
        })

    }
})

export const {increaseCounter,decreaseCounter,setCounter} = slice.actions
export default slice.reducer