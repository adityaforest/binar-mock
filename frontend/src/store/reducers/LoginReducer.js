import { createSlice } from "@reduxjs/toolkit";
import { loginThunk } from "../thunk/LoginThunk";
import authServices from "../../services/auth.services";

const slice = createSlice({
    name:'loginReducer',
    initialState:{
        loading:false,
        loggedIn:authServices.isLoggedIn(),
        errorMessage:''
    },
    reducers:{
        logout:(state,action) =>{
            authServices.logout()
            state.loggedIn = false
        }
    },
    extraReducers: builder => {
        builder.addCase(loginThunk.fulfilled, (state,action) => {
            state.loading = false
            console.log(action)
            // console.log('woi1')
            if(action.payload.status == "SUCCESS"){
                state.loggedIn = authServices.isLoggedIn()
                state.errorMessage = ''
            }
            else if(action.payload.status == "FAILED"){
                state.loggedIn = false
                state.errorMessage = action.payload.message
            }
        })
        builder.addCase(loginThunk.pending, (state,action) => {
            state.loading = true            
        })
        builder.addCase(loginThunk.rejected, (state,action) => {
            state.loading = false            
        })

    }
})

export const {logout} = slice.actions
export default slice.reducer