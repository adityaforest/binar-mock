import { configureStore } from "@reduxjs/toolkit";
import ExampleReducer from "./reducers/ExampleReducer";
import LoginReducer from "./reducers/LoginReducer";

export const store = configureStore({
    reducer:{
        ExampleReducer,
        LoginReducer
    }
})