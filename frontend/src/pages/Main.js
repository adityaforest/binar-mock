import React, { useEffect, useState } from 'react'
import styles from './Main.module.css'
import DateBox from '../components/DateBox'
import NewTaskModal from '../components/NewTaskModal'
import TaskDeleteModal from '../components/TaskDeleteModal'
import TaskEditModal from '../components/TaskEditModal'
import LogoutModal from '../components/LogoutModal'
import { useSelector } from 'react-redux'
import { useNavigate } from 'react-router-dom'
import taskServices from '../services/task.services'

function Main() {
  const today = new Date()
  const days = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday']
  const months = ['January', 'February', 'March', 'April', 'Mei', 'June', 'July', 'August', 'September', 'October', 'November', 'December']

  const [displayedDate, setDisplayedDate] = useState([])
  const [selectedDate, setSelectedDate] = useState(today.toDateString())
  const [showNewTaskModal, setShowNewTaskModal] = useState(false)
  const [showDeleteTaskModal, setShowDeleteTaskModal] = useState(false)
  const [showEditTaskModal, setShowEditTaskModal] = useState(false)
  const [showLogoutModal, setShowLogoutModal] = useState(false)

  const [displayedTasks, setDisplayedTasks] = useState([])
  const [selectedTask , setSelectedTask] = useState()
  const [isLoading, setIsLoading] = useState(false)

  const loggedIn = useSelector(state => state.LoginReducer.loggedIn)
  const navigate = useNavigate()

  useEffect(() => {
    fillDateSelector()
    refreshDisplayedTask()
  }, [])

  useEffect(() => {
    if (loggedIn == false) {
      navigate('/login')
    }
  }, [loggedIn])

  const fillDateSelector = () => {
    for (let i = -2; i <= 28; i++) {
      let target = new Date(today)
      target.setDate(today.getDate() + i).toLocaleString('en-us', { weekday: 'long' })
      setDisplayedDate(prev => {
        let old = [...prev]
        old.push({
          fullDate: target.toDateString(),
          day: days[target.getDay()],
          date: target.getDate(),
          month: months[target.getMonth()],
          year: target.getFullYear()
        })
        return old
      })
    }
  }

  const refreshDisplayedTask = async () => {
    const x = await taskServices.getAll(loggedIn.id).then(setIsLoading(true))
    if (x.data.status == 'FAILED') {
      alert(x.data.message)
    }
    else if (x.data.status == 'SUCCESS') {
      setDisplayedTasks(x.data.message)
    }
    setIsLoading(false)
  }

  const onChangeTaskStatus = async (task_id, detail, status) => {
    await taskServices.update(task_id, detail, status)
    refreshDisplayedTask()
  }

  const handleDateBoxClick = (fullDate) => {
    setSelectedDate(fullDate)
  }

  const newTaskModalShow = (e) => {
    setShowNewTaskModal(true)
  }

  const newTaskModalClose = (e) => {
    setShowNewTaskModal(false)
  }

  const deleteTaskModalShow = (task) => {
    setSelectedTask(task)
    setShowDeleteTaskModal(true)
  }

  const deleteTaskModalClose = (e) => {
    setShowDeleteTaskModal(false)
  }

  const editTaskModalShow = (task) => {
    setSelectedTask(task)
    setShowEditTaskModal(true)
  }

  const editTaskModalClose = (e) => {
    setShowEditTaskModal(false)
  }

  const logoutModalShow = (e) => {
    setShowLogoutModal(true)
  }

  const logoutModalClose = (e) => {
    setShowLogoutModal(false)
  }

  return (
    <div className={styles.main}>

      {showNewTaskModal ? <NewTaskModal show={showNewTaskModal} onClose={newTaskModalClose} selectedDate={selectedDate} refresh={refreshDisplayedTask}/> : null}
      {showDeleteTaskModal ? <TaskDeleteModal show={showDeleteTaskModal} onClose={deleteTaskModalClose} task={selectedTask} refresh={refreshDisplayedTask}/> : null}
      {showEditTaskModal ? <TaskEditModal show={showEditTaskModal} onClose={editTaskModalClose} task={selectedTask} refresh={refreshDisplayedTask}/> : null}
      {showLogoutModal ? <LogoutModal show={showLogoutModal} onClose={logoutModalClose} /> : null}

      <div className={styles.mainBox}>
        <div className='d-flex align-items-center justify-content-between text-white px-4' style={{ width: '100%' }}>
          <h6 >Your username : {loggedIn.username}</h6>
          <button className='bg-primary rounded' onClick={logoutModalShow}>Logout</button>
        </div>
        <div className='bg-dark'
          style={{ height: '700px', width: '160px', overflowX: 'hidden', overflowY: 'auto', transform: 'rotate(-90deg)', marginTop: '-260px', marginBottom: '-260px' }}>
          {
            displayedDate.map((date) => {
              return (
                <DateBox date={date} key={date?.fullDate} clickFunc={handleDateBoxClick}
                  selectedDate={selectedDate} />
              )
            })
          }
        </div>

        <h2 className='text-white m-0'>TO DO LIST</h2>
        <h6 className='text-white m-0'>{selectedDate}</h6>
        <div className='mt-2 ' style={{ width: '650px', height: '200px', overflow: 'auto' }}>
          {
            displayedTasks.length > 0 ?
              displayedTasks
                .filter((x) => x.fullDate == selectedDate)
                .sort(({ id: previousID }, { id: currentID }) => previousID - currentID)
                .map(task => {
                  return (
                    <div className='bg-dark rounded mt-3 text-white px-2 d-flex align-item-center' style={{ width: '600px', height: '30px' }}>
                      <div className='' style={{ height: '100%', width: '65%' }} key={task.id}>
                        <p style={{ whiteSpace: 'nowrap', overflow: 'hidden', textOverflow: 'ellipsis' }}>{task.detail}</p>
                      </div>
                      <div className='d-flex align-items-center justify-content-end' style={{ height: '100%', width: '35%' }}>
                        <select className='bg-primary text-white rounded mx-1' style={{ height: '25px' }}
                          value={task.status} onChange={e => onChangeTaskStatus(task.id, task.detail, e.target.value)}>
                          <option value="To-Do">To-Do</option>
                          <option value="Done">Done</option>
                        </select>
                        <button className='bg-primary text-white rounded mx-1' style={{ height: '25px' }} onClick={(e) => editTaskModalShow(task)}>Edit</button>
                        <button className='bg-danger text-white rounded mx-1' style={{ height: '25px' }} onClick={(e) => deleteTaskModalShow(task)}>Delete</button>
                      </div>
                    </div>
                  )
                })
              :
              <div className='d-flex justify-content-center align-items-center' style={{ width: '100%', height: '100%' }}>
                <h5>No task created ...</h5>
              </div>
          }

        </div>
        <div className='d-flex justify-content-end' style={{ width: '100%' }}>
          <button className='bg-primary rounded mt-2 mx-5 text-white'
            onClick={newTaskModalShow}>Create New Task</button>
        </div>
      </div>
    </div>
  )
}

export default Main