import { useState } from 'react';
import { Modal, Button } from 'react-bootstrap';
import taskServices from '../services/task.services';

function TaskEditModal({ show, onClose , task, refresh}) {
    const [detail, setDetail] = useState(task.detail)

    const onDetailChange = (e) => {
        e.preventDefault()
        setDetail(e.target.value)
    }

    const editTask = async () => {
        await taskServices.update(task.id,detail,task.status)
        onClose()
        refresh()
    }

    return (
        <Modal className="" show={show} onClose={onClose}>
            <Modal.Header >
                <Modal.Title>Edit Task</Modal.Title>
            </Modal.Header>

            <Modal.Body>
                <input type="text" value={detail} onChange={onDetailChange}
                    style={{ width: '100%' }} />
            </Modal.Body>

            <Modal.Footer>
                <Button variant="danger" onClick={onClose}>Cancel</Button>
                <Button variant="primary" onClick={editTask}>Edit</Button>
            </Modal.Footer>
        </Modal>
    );
}
export default TaskEditModal;  