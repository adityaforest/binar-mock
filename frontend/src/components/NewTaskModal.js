import { Modal, Button } from 'react-bootstrap';
import taskServices from '../services/task.services';
import authServices from '../services/auth.services'
import { useState } from 'react';

function NewTaskModal({show,onClose,selectedDate,refresh}) {
    const [detail,setDetail] = useState('')
    const [isLoading,setIsLoading] = useState(false)

    const onDetailChange = (e) => {
        e.preventDefault()
        setDetail(e.target.value)
    }

    const createNewTask = async (e) => {
        e.preventDefault()
        const x = await taskServices.create(
            authServices.isLoggedIn().id,
            detail,
            "To-Do",
            selectedDate
        ).then(setIsLoading(true))

        if(x.data.status == 'SUCCESS'){
            setIsLoading(false)
            onClose()
            refresh()
        }
        else if(x.data.status == 'FAILED'){
            setIsLoading(false)
            alert(x.data.message)
        }        
    }
    return (
        <Modal className="" show={show} onClose={onClose}>            
                <Modal.Header >
                    <Modal.Title>Create New Task</Modal.Title>
                </Modal.Header>

                <Modal.Body>
                    <input type="text" placeholder='Enter what do you want to do ...' value={detail} onChange={onDetailChange}
                    style={{ width:'100%' }}/>
                </Modal.Body>

                <Modal.Footer>
                    <Button variant="danger" onClick={onClose} disabled={isLoading}>Cancel</Button>
                    <Button variant="primary" onClick={createNewTask} disabled={isLoading}>Create</Button>
                </Modal.Footer>            
        </Modal>
    );
}
export default NewTaskModal;  