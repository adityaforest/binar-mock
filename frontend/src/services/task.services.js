import axios from 'axios';
import authHeader from './auth.header';

const API_URL = `${process.env.REACT_APP_BE_URL}/api/v1/tasks/`;

class OtherServices {  

  getAll(userid) {
    return axios.get(API_URL + `getAll/${userid}`, { headers: authHeader() });
  }

  create(user_id,detail,status,fullDate) {
    return axios.post(API_URL + 'create',{user_id,detail,status,fullDate} ,{ headers: authHeader() });
  }

  update(task_id,detail,status){
    return axios.put(API_URL + `update/${task_id}`,{detail,status},{ headers: authHeader() })
  }

  delete(task_id){
    return axios.delete(API_URL+`delete/${task_id}`,{ headers: authHeader() })
  }

}

export default new OtherServices();