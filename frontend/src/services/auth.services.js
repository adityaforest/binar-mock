import axios from 'axios'

//don't forget to create .env.local files with REACT_APP variables
const API_URL = `${process.env.REACT_APP_BE_URL}/api/v1/auth/`

class AuthServices {
    login(uniqueNumber) {
        return axios
            .post(API_URL + "login", { uniqueNumber })
            .then((response) => {
                // console.log(response)
                if (response.data.message.token) {
                    localStorage.setItem("user", JSON.stringify(response.data.message));
                }                
                return response.data;
            })
            .catch((response) => {                
                return response.response.data
            })
    }

    logout() {
        localStorage.removeItem('user')
    }   

    isLoggedIn() {        
        const user = JSON.parse(localStorage.getItem("user"));
                
        if (user) {
            return user
        }        
        else {
            return false
        }
    }
}

export default new AuthServices